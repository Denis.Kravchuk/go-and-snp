﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskScheduler;

namespace Lab04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FormRefresh();
        }

        private void FormRefresh()
        {
            GetAllUsersPrograms();
            GetCurrentUserPrograms();
            GetAllUsersScheduledTasks();
        }

        private void GetAllUsersPrograms()
        {
            listBox1.Items.Clear();
            var services = Registry.LocalMachine
                .OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run")
                .GetValueNames()
                .ToList();
            for(int i = 0; i < services.Count; i++)
            {
                if(services[i] != "")
                    listBox1.Items.Add(services[i]);
            }
        }
        
        private void GetCurrentUserPrograms()
        {
            listBox2.Items.Clear();
            var services = Registry.CurrentUser
                .OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run")
                .GetValueNames()
                .ToList();
            for (int i = 0; i < services.Count; i++)
            {
                if (services[i] != "")
                    listBox2.Items.Add(services[i]);
            }
        }

        private void GetAllUsersScheduledTasks()
        {
            listBox3.Items.Clear();

            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey keySchedule = key.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tree\");
            List<string> schedule = keySchedule.GetSubKeyNames().ToList();
            for (int i = 0; i < schedule.Count; i++)
            {
                listBox3.Items.Add(schedule[i]);
            }
        }

        private void buttonAddAutoProgramm_Click(object sender, EventArgs e)
        {
            var toSetAutorun = textBox1.Text.ToString();
            if (toSetAutorun != "")
                using (var key = Registry.CurrentUser
                    .OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true))
                    key.SetValue(toSetAutorun, "\"" + toSetAutorun + "\"");
            FormRefresh();
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            string key = textBox2.Text;
            var dateNow = DateTime.Now;
            var path = $@"D:\University_3-course_2-semester\system programming\Лаб-04\Copy.reg";


            string arguments = $"reg export \"{key}\" \"{path}\" /y";
            string strCmdText = "/C " + arguments;
            const string FILE_NAME = "CMD.exe";

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = FILE_NAME,
                    Arguments = strCmdText,
                    UseShellExecute = false,
                    CreateNoWindow = false,
                }
            };

            try
            {
                process.Start();
                if (process != null)
                    process.WaitForExit();
            }
            finally { if (process != null) { process.Dispose(); } }

            MessageBox.Show("Копіювання завершено!");

            textBox2.Text = "";
        }

        private void buttonTtt_Click(object sender, EventArgs e)
        {
            var path = $@"D:\University_3-course_2-semester\system programming\Лаб-04\ttt.reg";
            string arguments = $"/s {path}";
            var regeditProcess = Process.Start($"regedit.exe", arguments);
            regeditProcess.WaitForExit();
        }
    }
}
