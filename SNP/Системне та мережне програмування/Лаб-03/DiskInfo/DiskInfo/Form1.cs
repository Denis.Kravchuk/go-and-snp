﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiskInfo
{
    public partial class Form1 : Form
    {
        protected readonly PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                TrackingChanges(folderBrowserDialog1.SelectedPath);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ShowInfo();
        }

        public void ShowInfo()
        {
            //get logical drives to listbox1
            listBox1.Items.Add("List of ligical drives:");
            foreach (var drive in DriveInfo.GetDrives())
            {
                listBox1.Items.Add(drive.Name);
                string diskType = "";
                if(drive.DriveType.ToString() == "CDRom")
                {
                    diskType = "Type: CDRom. The drive is an optical disc device, such as a CD or DVD-ROM.";
                }
                else if (drive.DriveType.ToString() == "Fixed")
                {
                    diskType = "Type: Fixed. The drive is a fixed disk.";
                }
                else if (drive.DriveType.ToString() == "Network")
                {
                    diskType = "Type: Network. The drive is a network drive.";
                }
                else if (drive.DriveType.ToString() == "NoRootDirectory")
                {
                    diskType = "Type: NoRootDirectory. The drive does not have a root directory.";
                }
                else if (drive.DriveType.ToString() == "Ram")
                {
                    diskType = "Type: Ram. The drive is a RAM disk.";
                }
                else if (drive.DriveType.ToString() == "Removable")
                {
                    diskType = "Type: Removable. The drive is a removable storage device, such as a USB flash drive.";
                }
                else
                {
                    diskType = "The type of drive is unknown.";
                }

                listBox1.Items.Add(diskType);
                listBox1.Items.Add($"File system: {drive.DriveFormat}");

                listBox1.Items.Add($"Available space to current user: {drive.AvailableFreeSpace / 1024 / 1024 / 1024} Gb");
                listBox1.Items.Add($"Total available space: {drive.TotalFreeSpace / 1024 / 1024 / 1024} Gb");
                listBox1.Items.Add($"Total size of drive: {drive.TotalSize / 1024 / 1024 / 1024} Gb");
            }

            //get system info to listbox2
            listBox2.Items.Add("System info:");
            listBox2.Items.Add($"System memory: {ramCounter.NextValue()} Mb");
            listBox2.Items.Add($"Machine name: {Environment.MachineName}");
            listBox2.Items.Add($"User name: {SystemInformation.UserName}");

            //get directory info to listbox3
            listBox3.Items.Add("DEirectory info:");
            DirectoryInfo systemDir = new DirectoryInfo(Environment.SystemDirectory);
            DirectoryInfo tempDir = new DirectoryInfo(Path.GetTempPath());
            DirectoryInfo currentDir = new DirectoryInfo(Directory.GetCurrentDirectory());

            listBox3.Items.Add($"System directory: {Environment.CurrentDirectory = (Environment.SystemDirectory)}");
            listBox3.Items.Add($"Name of directory: {systemDir.Name}");
            listBox3.Items.Add($"Full mame of directory: {systemDir.FullName}");
            listBox3.Items.Add($"Creation time of directory: {systemDir.CreationTime}");
            listBox3.Items.Add($"Root of directory: {systemDir.Root}");
            listBox3.Items.Add(" ");
            listBox3.Items.Add($"Temporary directory: {Environment.CurrentDirectory = (Path.GetTempPath())}");
            listBox3.Items.Add($"Name of directory: {tempDir.Name}");
            listBox3.Items.Add($"Full mame of directory: {tempDir.FullName}");
            listBox3.Items.Add($"Creation time of directory: {tempDir.CreationTime}");
            listBox3.Items.Add($"Root of directory: {tempDir.Root}");
            listBox3.Items.Add(" ");
            listBox3.Items.Add($"Current directory: {Environment.CurrentDirectory = (Directory.GetCurrentDirectory())}");
            listBox3.Items.Add($"Name of directory: {currentDir.Name}");
            listBox3.Items.Add($"Full mame of directory: {currentDir.FullName}");
            listBox3.Items.Add($"Creation time of directory: {currentDir.CreationTime}");
            listBox3.Items.Add($"Root of directory: {currentDir.Root}");
        }

        public void TrackingChanges(string path)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Filter = "*.*";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnRenamed);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            string path = @"D:\University_3-course_2-semester\system programming\Лаб-03\log.txt";
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine($"File {e.OldFullPath} was renamed to {e.FullPath}");
            }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            string path = @"D:\University_3-course_2-semester\system programming\Лаб-03\log.txt"; ;
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine($"File: {e.FullPath} {e.ChangeType}");
            }
        }
    }
}
