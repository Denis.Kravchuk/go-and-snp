﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NumsViewer
{
    public unsafe partial class Form1 : Form
    {
        static Mutex mutex;        

        public Form1()
        {
            InitializeComponent();
        }

        private unsafe void Form1_Load(object sender, EventArgs e)
        {
            bool res = false;
            while (res==false) {
                res = Mutex.TryOpenExisting("Mutex", out mutex);
            }
            timer1.Start();            
        }     

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                mutex.WaitOne();
                listBox1.Items.Clear();
                var mmf = MemoryMappedFile.OpenExisting("fileHandle");
                var accessor = mmf.CreateViewAccessor();
                byte* pointer = null;

                //Забезпечує безпечний дескриптор (блок некерованої пам’яті для довільного доступу)
                accessor.SafeMemoryMappedViewHandle.AcquirePointer(ref pointer);
                string textElement;
                for (int i = 0; i < 20; i++)
                {
                    textElement = "";
                    for (int j = 0; j < (*(int*)(pointer + i * 4)); j++)
                        textElement += "*";
                    listBox1.Items.Add(textElement);
                }
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }
    }    
}
