﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static Mutex mutex;
        static unsafe void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Random rnd = new Random();
            int[] arr = new int[20];

            //створення файлу розміром 1024 КБ
            var mmf = MemoryMappedFile.CreateFromFile(@"../../../data.dat", FileMode.Create, "fileHandle", 1024 * 1024);
            
            //доступ до файлу, що відображений в пам'яті
            var accessor = mmf.CreateViewAccessor();
            while (true)
            {
                Console.WriteLine("Натисніть пробіл для генерації файлу: ");
                ConsoleKeyInfo key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.Spacebar) // коли натискаємо пробіл
                {
                    mutex = new Mutex(false, "Mutex");

                    try
                    {
                        mutex.WaitOne();
                        for (int i = 0; i < arr.Length; i++)
                        {
                            arr[i] = rnd.Next(10, 100);
                        }
                        
                        accessor.WriteArray(0, arr, 0, arr.Length);
                        Console.WriteLine("Файл згенеровано!");
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }
            }
        }        
    }    
}
