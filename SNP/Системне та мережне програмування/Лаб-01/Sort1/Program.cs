﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sort
{
    class Program
    {        
        static Mutex mutex;

        static unsafe void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            bool res = false;
            while (res == false)
            {
                res = Mutex.TryOpenExisting("Mutex", out mutex); 
            }
            bool checkBubbleSortEnd;
            int c;
            var mmf = MemoryMappedFile.OpenExisting("fileHandle");
            var accessor = mmf.CreateViewAccessor();
            byte* pointer = null;
            //Забезпечує безпечний дескриптор (блок некерованої пам’яті для довільного доступу)
            accessor.SafeMemoryMappedViewHandle.AcquirePointer(ref pointer);
            while (true)
            {
                Console.WriteLine("Натисніть пробіл для початку сортування: ");
                ConsoleKeyInfo key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.Spacebar)
                {                       
                    do
                    {
                        checkBubbleSortEnd = false;
                        for (int i = 1; i < 20; i++)
                        {
                            try
                            {
                                mutex.WaitOne();
                                if (*(int*)(pointer + (i - 1) * 4) > *(int*)(pointer + i * 4))
                                {
                                    c = *(int*)(pointer + i * 4);
                                    *(int*)(pointer + i * 4) = *(int*)(pointer + (i - 1) * 4);
                                    *(int*)(pointer + (i - 1) * 4) = c;
                                    checkBubbleSortEnd = true;
                                    Thread.Sleep(1500);
                                }
                            }
                            finally
                            {
                                mutex.ReleaseMutex();
                            }
                        }
                    } while (checkBubbleSortEnd);
                    break;
                }
            }
        }
    }
}
