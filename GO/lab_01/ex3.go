package main

import "fmt"

func main() {
	//Инициализация переменных
	var userinit8 uint8 = 1
	var userinit16 uint16 = 2
	var userinit64 int64 = -3
	var userautoinit = -4 //Такой вариант инициализации также возможен

	fmt.Println("Values: ", userinit8, userinit16, userinit64, userautoinit, "\n")

	//Задание.
	//1. Вывести типы всех переменных
	fmt.Printf("Value of userinit8 = %d Type = %T\n", userinit8, userinit8)
	fmt.Printf("Value of userinit16 = %d Type = %T\n", userinit16, userinit16)
	fmt.Printf("Value of userinit64 = %d Type = %T\n", userinit64, userinit64)
	fmt.Printf("Value of userautoinit = %d Type = %T\n", userautoinit, userautoinit)

	//2. Присвоить переменной intVar переменные userinit16 и userautoinit. Результат вывести.

	//Краткая запись объявления переменной
	//только для новых переменных

	intVar := -10
	intVar2 := userautoinit
	fmt.Printf("\nValue of intVar = %d Type = %T\n", intVar, intVar)
	fmt.Printf("\nValue of intVar2 = %d Type = %T\n", intVar2, intVar2)
}
