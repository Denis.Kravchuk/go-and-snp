/*Многострочный комментарий
Структура программы */
package main

//Однострочный комментарий
//Импорт пакетов
import (
	"fmt"
	"math/rand"
)

func main() {
	//Объявление переменной
	var str string = "Golang!"

	//Вывод в консоль текста
	fmt.Println("Hello ", str)

	//Задание.
	//1. Вывести текст на украинском языке
	var str2 string = "світ!"

	//Вывод в консоль текста
	fmt.Println("Привіт, ", str2)
}
