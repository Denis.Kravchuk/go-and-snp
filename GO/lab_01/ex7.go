package main

import "fmt"

func main() {
	variable8 := int8(127)
	variable16 := int16(16383)

	fmt.Println("Приведение типов\n")

	fmt.Printf("variable8         = %-5d = %.16b\n", variable8, variable8)
	fmt.Printf("variable16        = %-5d = %.16b\n", variable16, variable16)
	fmt.Printf("uint16(variable8) = %-5d = %.16b\n", uint16(variable8), uint16(variable8))
	fmt.Printf("uint8(variable16) = %-5d = %.16b\n", uint8(variable16), uint8(variable16))

	//Задание.
	//1. Создайте 2 переменные  разных типов. Выполните арифметические операции. Результат вывести
	myVariable1 := int32(1234)
	myVariable2 := int64(3100234)

	fmt.Printf("\nmyVariable1 + myVariable2 = %d\n", int64(myVariable1)+int64(myVariable2))
	fmt.Printf("myVariable1 - myVariable2 = %d\n", int64(myVariable1)-int64(myVariable2))
	fmt.Printf("myVariable1 / myVariable2 = %d\n", int64(myVariable1)/int64(myVariable2))
	fmt.Printf("myVariable1 * myVariable2 = %d\n", int64(myVariable1)*int64(myVariable2))
}
