package main

import (
	"fmt"
	mymath "go/lab_02/math"
)

func main() {
	min := mymath.Min(1.3, -2.33, 3.2)
	fmt.Println("Мінімальне число серед 1.3, -2.33, 3.2 = ", min)

	average := mymath.Average(1, 55, -20)
	fmt.Println("Середнє арифметичне серед 1, 55, -20 = ", average)

	x := mymath.X(2, 10)
	fmt.Println("x з рівняння 2x+10=0 становить ", x)
}
