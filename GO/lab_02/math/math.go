package math

func Min(x1 float64, x2 float64, x3 float64) float64 {
	if x1 < x2 && x1 < x3 {
		return x1
	} else if x2 < x1 && x2 < x3 {
		return x2
	}
	return x3
}

func Average(x1 float64, x2 float64, x3 float64) float64 {
	return (x1 + x2 + x3) / 3
}

func X(a float64, b float64) float64 {
	return -b / a
}
