package math

import "testing"

func TestMin(t *testing.T) {
	x := Min(1, 2, 3)
	var res float64 = 1
	if x != res {
		t.Errorf("Тест не пройдено! Результат %f, а мав бути %f", x, res)
	}
}

func TestAverage(t *testing.T) {
	x := Average(1, 55, -20)
	var res float64 = 12
	if x != res {
		t.Errorf("Тест не пройдено! Результат %f, а мав бути %f", x, res)
	}
}

func TestX(t *testing.T) {
	x := X(1, -1)
	var res float64 = 1
	if x != res {
		t.Errorf("Тест не пройдено! Результат %f, а мав бути %f", x, res)
	}
}
