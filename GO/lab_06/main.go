package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

func NewClient(name string, surname string, acc_num string, deposit float64, credits float64) Client {
	return Client{name, surname, acc_num, deposit, credits, nil}
}
func (this *Client) SetName(n string) {
	this.Name = n
}
func (this *Client) SetSurName(n string) {
	this.Surname = n
}
func (this *Client) SetDeposit(d float64) {
	this.c_Deposit = d
}
func (this *Client) _AddToDeposit(d float64) {
	this.c_Deposit += d
	this.Bank._AddToDeposit(d)
}
func (this *Client) _SubFromDeposit(d float64) {
	this.c_Deposit -= d
	this.Bank._SubFromDeposit(d)
}
func (this *Client) SetCredit(c float64) {
	this.c_Credit = c
}

func (this *Client) _AddToCredit(c float64) {
	this.c_Credit -= c
	this.Bank._AddToCredit(c)
}
func (this *Client) _SubFromCredit(c float64) {
	this.c_Credit += c
	this.Bank._SubFromCredit(c)
}
func (this Client) GetName() string {
	return this.Name
}
func (this Client) GetSurName() string {
	return this.Surname
}
func (this Client) GetFullName() string {
	return this.Name + " " + this.Surname
}
func (this Client) GetDeposit() float64 {
	return this.c_Deposit
}
func (this Client) GetCredit() float64 {
	return this.c_Credit
}
func (this Client) PrintClientInfo() {
	fmt.Printf("%-10s %-10.2f %-10.2f %-10s\n", this.GetFullName(), this.c_Deposit, this.c_Credit, this.Account_Number)
}
func (this *Client) AddToDeposit(money float64) {
	this._AddToDeposit(money)
}
func (this *Client) TakeFromDeposit(money float64) {
	if !this.CheckMoney(money) {
		return
	}
	this._SubFromDeposit(money)
}
func (this *Client) PayCredit(money float64) {
	if money < 0 {
		return
	}
	if !this.CheckMoney(money) {
		return
	}

	if money > -this.GetCredit() { //якщо є більше грошей, ніж кредит
		extra := money + this.GetCredit()
		money -= extra
	}
	this._SubFromDeposit(money)
	this._SubFromCredit(money)
}
func (this *Client) TakeCredit(money float64) {
	if !this.Bank.CheckMoney(money) {
		return
	}
	this._AddToDeposit(money)
	this._AddToCredit(money)

}

// перевірка, чи клієнт має не менше грошей на депозиті
func (this *Client) CheckMoney(m float64) bool {
	if this.GetDeposit() < m {
		fmt.Printf("%s, у вас немає стільки грошей: %.3f\n", this.GetFullName(), m)
		fmt.Printf("Ви маєте: %.3f\n", this.GetDeposit())
		return false
	}
	return true
}

type Client struct {
	Name           string
	Surname        string
	Account_Number string
	c_Deposit      float64
	c_Credit       float64
	Bank           *Bank
}

func NewBank(name string, bank_money float64) Bank {
	return Bank{name, bank_money, 0, 0, []*Client{}}
}
func (this *Bank) SetName(n string) {
	this.Name = n
}
func (this *Bank) SetBankMoney(m float64) {
	this.Bank_Money = m
}
func (this *Bank) SetDeposit(d float64) {
	this.Deposits = d
}
func (this *Bank) SetCredit(c float64) {
	this.Credits = c
}
func (this Bank) GetDeposit() float64 {
	return this.Deposits
}
func (this Bank) GetBankMoney() float64 {
	return this.Bank_Money
}
func (this Bank) GetClient(index int) *Client {
	return this.Clients[index]
}
func (this *Bank) GetClientByAccountNumber(acc_num string) {
	for _, c := range this.Clients {
		if c.Account_Number == acc_num {
			c.PrintClientInfo()
		}
	}
}
func (this *Bank) GetClientBySurname(surname string) {
	for _, c := range this.Clients {
		if c.Surname == surname {
			c.PrintClientInfo()
		}
	}
}

func (this *Bank) AddClient(c *Client) {
	this.Clients = append(this.Clients, c)
	this._AddToDeposit(c.c_Deposit)
	this._AddToCredit(-c.c_Credit)
	c.Bank = this
}
func (this *Bank) RemoveClient(index int) {
	this.Clients = append(this.Clients[:index], this.Clients[index+1:]...)
}
func (this *Bank) PrintClients() {
	fmt.Printf("%-10s %-10s %-10s %-10s\n", "Банк", "Депозити", "Кредити", "Гроші банку")
	fmt.Printf("%-10s %-10.2f %-10.2f %-10.2f\n", this.Name, this.Deposits, this.Credits, this.Bank_Money)
	fmt.Printf("%-30s %s\n", "Клієнт", "Номер аккаунту")
	for _, c := range this.Clients {
		c.PrintClientInfo()
	}
}
func (this *Bank) _AddToDeposit(m float64) {
	this.Deposits = this.Deposits + m
}
func (this *Bank) _SubFromDeposit(m float64) {
	this.Deposits -= m
}
func (this *Bank) _AddToCredit(m float64) {
	this.Credits += m
	this._TakeMoney(m)
}
func (this *Bank) _SubFromCredit(m float64) {
	this.Credits -= m
	this._AddMoney(m)
}
func (this *Bank) _AddMoney(m float64) {
	this.Bank_Money += m
}
func (this *Bank) _TakeMoney(m float64) {
	this.Bank_Money -= m
}

//якщо у банку є достатньо грошей на депозиті, повертає true
func (this *Bank) CheckMoney(m float64) bool {
	if this.GetBankMoney() < m {
		return false
	}
	return true
}

type Bank struct {
	Name       string
	Bank_Money float64
	Deposits   float64
	Credits    float64
	Clients    []*Client
}

func main() {
	is_bank_created := false
	bank := Bank{}
	ch := make(chan struct{}, 1)
	go func() {
		ch <- struct{}{}
	}()

	for true {
		var choice int
		var tmp string
		var err error
		fmt.Println("1: Створити банк\n2: Додати клієнта для кредитів\n3: Додати клієнта для депозитів")
		fmt.Println("4: Вивести інформацію про клієнта за номером аккаунту")
		fmt.Println("5: Вивести інформацію про клієнта за прізвищем")
		fmt.Println("6: Вивести інформацію про усіх клієнтів")
		fmt.Println("0: Вихід")
		for true {
			fmt.Print("-> ")
			_, _ = fmt.Scanln(&tmp)
			choice, err = strconv.Atoi(tmp)
			if err != nil || choice < 0 || choice > 6 {
				fmt.Println("Неправильний ввід.")
			} else {
				break
			}
		}
		switch choice {
		case 0:
			return
		case 1:
			{
				if is_bank_created {
					fmt.Printf("Банк з такою назвою уже існує: %s\n", bank.Name)
					break
				}
				var (
					bank_name  string
					bank_money float64
				)

				fmt.Print("Введіть назву банку: ")
				_, _ = fmt.Scanln(&bank_name)
				for bank_name == "" {
					fmt.Println("Введіть назву банку ще раз: ")
					_, _ = fmt.Scanln(&bank_name)
				}
				for true {
					fmt.Print("Введіть к-сть грошей у банку:  ")
					_, _ = fmt.Scanln(&tmp)
					bank_money, err = strconv.ParseFloat(tmp, 64)
					if err != nil || bank_money <= 0 {
						fmt.Println("Введіть к-сть грошей у банку знову:  ")
					} else {
						break
					}
				}
				bank = NewBank(bank_name, bank_money)
				is_bank_created = true
			}
		case 2, 3:
			{
				if !is_bank_created {
					fmt.Println("Банк не створено")
					break
				}

				var (
					name    string
					surname string
					acc_num string
				)
				fmt.Print("Введіть ім'я клієнта: ")
				_, _ = fmt.Scanln(&name)
				for name == "" {
					fmt.Println("Введіть ім'я клієнта ще раз: ")
					_, _ = fmt.Scanln(&name)
				}
				fmt.Print("Введіть прізвище клієнта: ")
				_, _ = fmt.Scanln(&surname)
				for surname == "" {
					fmt.Println("Введіть прізвище клієнта ще раз: ")
					_, _ = fmt.Scanln(&surname)
				}
				fmt.Print("Введіть номер аккаунту клієнта: ")
				_, _ = fmt.Scanln(&acc_num)
				for acc_num == "" {
					fmt.Println("Введіть номер аккаунту клієнта знову: ")
					_, _ = fmt.Scanln(&acc_num)
				}
				if choice == 2 {
					client := NewClient(name, surname, acc_num, 500, -500)
					go func() {
						<-ch
						bank.AddClient(&client)
						ch <- struct{}{}
						go ClientCreditWork(&client, ch)
					}()
				} else {
					client := NewClient(name, surname, acc_num, 500, -10)
					go func() {
						<-ch
						bank.AddClient(&client)
						ch <- struct{}{}
						go ClientDepositWork(&client, ch)
					}()
				}

			}
		case 4:
			{
				if !is_bank_created {
					fmt.Println("Банк не створено.")
					break
				}
				acc_num := ""
				fmt.Print("Введіть номер аккаунту клієнта: ")
				fmt.Scanln(&acc_num)

				go func() {
					<-ch
					bank.GetClientByAccountNumber(acc_num)
					ch <- struct{}{}
				}()

			}
		case 5:
			{
				if !is_bank_created {
					fmt.Println("Банк не створено.")
					break
				}
				surname := ""
				fmt.Print("Введіть прізвище клієнта: ")
				fmt.Scanln(&surname)

				go func() {
					<-ch
					bank.GetClientBySurname(surname)
					ch <- struct{}{}
				}()
			}
		case 6:
			{
				if !is_bank_created {
					fmt.Println("Банк не створено.")
					break
				}
				go func() {
					<-ch
					bank.PrintClients()
					ch <- struct{}{}
				}()

			}

		}
		fmt.Println("Натисніть ентер...")
		_, _ = fmt.Scanln()
	}

}

func ClientDepositWork(c *Client, ch chan struct{}) {
	for c.GetDeposit() > 0 {
		choice := rand.Intn(2) + 1
		<-ch
		switch choice {
		case 1:
			{
				c.AddToDeposit(100)
				break
			}
		case 2:
			{
				c.TakeFromDeposit(100)
				break
			}
		}
		ch <- struct{}{}
		time.Sleep(time.Second)
	}
	fmt.Println(c.GetFullName() + " завершено роботу з депозитами\n")
}

func ClientCreditWork(c *Client, ch chan struct{}) {
	for c.GetCredit() < 0 {
		choice := rand.Intn(2) + 1
		<-ch
		switch choice {
		case 1:
			{
				c.TakeCredit(100)
				break
			}
		case 2:
			{
				c.PayCredit(100)
				break
			}
		}
		ch <- struct{}{}
		time.Sleep(time.Second)
	}
	fmt.Println(c.GetFullName() + " завершено роботу з кредитами\n")
}
