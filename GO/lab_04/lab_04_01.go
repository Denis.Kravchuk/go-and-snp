package main

import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func main() {
	err := ui.Main(func() {
		window := ui.NewWindow("Калькулятор склопакета", 500, 300, false)

		box := ui.NewVerticalBox()
		boxUp := ui.NewHorizontalBox()
		boxDown := ui.NewHorizontalBox()
		boxLeft := ui.NewVerticalBox()
		boxRight := ui.NewVerticalBox()

		box1 := ui.NewHorizontalBox()
		box2 := ui.NewHorizontalBox()
		box3 := ui.NewHorizontalBox()

		label1 := ui.NewLabel("Розмір вікна")
		label2 := ui.NewLabel("Ширина, см")
		input1 := ui.NewEntry()
		input2 := ui.NewEntry()

		box1.Append(label1, true)
		box1.Append(input1, true)
		box2.Append(label2, true)
		box2.Append(input2, true)

		label3 := ui.NewLabel("Матеріал")
		material := ui.NewCombobox()
		material.Append("Дерево")
		material.Append("Метал")
		material.Append("Металопластик")
		material.SetSelected(0)

		box3.Append(label3, true)
		box3.Append(material, true)

		boxLeft.Append(box1, false)
		boxLeft.Append(box2, false)
		boxLeft.Append(box3, false)

		label4 := ui.NewLabel("Склопакет")
		combobox := ui.NewCombobox()
		combobox.Append("Однокамерний")
		combobox.Append("Двокамерний")
		combobox.SetSelected(0)

		checkbox := ui.NewCheckbox("Підвіконня")
		checkbox.SetChecked(true)

		boxRight.Append(label4, false)
		boxRight.Append(combobox, false)
		boxRight.Append(checkbox, false)

		label5 := ui.NewLabel("")
		button := ui.NewButton("Розрахувати")
		boxDown.Append(label5, true)
		boxDown.Append(button, true)

		boxUp.Append(boxLeft, true)
		boxUp.Append(boxRight, true)
		box.Append(boxUp, true)
		box.Append(boxDown, false)

		box.SetPadded(true)
		boxLeft.SetPadded(true)
		boxRight.SetPadded(true)
		boxUp.SetPadded(true)

		window.SetMargined(true)
		window.SetChild(box)

		button.OnClicked(func(*ui.Button) {
			width, err := strconv.ParseFloat(input1.Text(), 2)
			height, err := strconv.ParseFloat(input2.Text(), 2)

			area := width * height
			result := 0.0
			if err == nil {
				if checkbox.Checked() == true {
					result += 350
				}
				if material.Selected() == 0 && combobox.Selected() == 0 {
					result += area * 2.5
				} else if material.Selected() == 0 && combobox.Selected() == 1 {
					result += area * 3
				} else if material.Selected() == 1 && combobox.Selected() == 0 {
					result += area * 0.5
				} else if material.Selected() == 1 && combobox.Selected() == 1 {
					result += area * 1
				} else if material.Selected() == 2 && combobox.Selected() == 0 {
					result += area * 1.5
				} else if material.Selected() == 2 && combobox.Selected() == 1 {
					result += area * 2
				}
				resultString := fmt.Sprintf("%.2f", result)
				resultString += " грн."
				label5.SetText(resultString)
			}

		})

		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})
		window.Show()
	})
	if err != nil {
		panic(err)
	}
}
