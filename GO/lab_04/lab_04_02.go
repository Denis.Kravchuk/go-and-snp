package main

import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func main() {
	err := ui.Main(func() {
		window := ui.NewWindow("Калькулятор вартості туру", 400, 200, false)

		box := ui.NewVerticalBox()

		box1 := ui.NewHorizontalBox()
		box2 := ui.NewHorizontalBox()
		box3 := ui.NewHorizontalBox()
		box4 := ui.NewHorizontalBox()
		box5 := ui.NewHorizontalBox()
		box6 := ui.NewHorizontalBox()

		label1 := ui.NewLabel("К-сть днів:")
		input1 := ui.NewEntry()
		box1.Append(label1, true)
		box1.Append(input1, true)

		label2 := ui.NewLabel("Країна")
		country := ui.NewCombobox()
		country.Append("Болгарія")
		country.Append("Німеччина")
		country.Append("Польща")
		country.SetSelected(0)
		box2.Append(label2, true)
		box2.Append(country, true)

		label3 := ui.NewLabel("Пора року")
		season := ui.NewCombobox()
		season.Append("Літо")
		season.Append("Зима")
		season.SetSelected(0)
		box3.Append(label3, true)
		box3.Append(season, true)

		guide := ui.NewCheckbox("Персональний гід")
		guide.SetChecked(false)
		box4.Append(guide, true)

		luxe := ui.NewCheckbox("Номер люкс")
		luxe.SetChecked(false)
		box5.Append(luxe, true)

		label4 := ui.NewLabel("")
		button := ui.NewButton("Розрахувати")
		box6.Append(label4, true)
		box6.Append(button, true)

		box.Append(box1, false)
		box.Append(box2, false)
		box.Append(box3, false)
		box.Append(box4, false)
		box.Append(box5, false)
		box.Append(box6, false)

		window.SetMargined(true)
		window.SetChild(box)

		button.OnClicked(func(*ui.Button) {
			days, err := strconv.ParseFloat(input1.Text(), 2)
			result := 0.0
			if err == nil {
				if country.Selected() == 0 && season.Selected() == 0 {
					result = days * 100
				} else if country.Selected() == 0 && season.Selected() == 1 {
					result = days * 150
				} else if country.Selected() == 1 && season.Selected() == 0 {
					result = days * 160
				} else if country.Selected() == 1 && season.Selected() == 1 {
					result = days * 200
				} else if country.Selected() == 2 && season.Selected() == 0 {
					result = days * 120
				} else if country.Selected() == 2 && season.Selected() == 1 {
					result = days * 180
				}

				if luxe.Checked() == true {
					result *= 1.2
				}
				if guide.Checked() == true {
					result += days * 50
				}

				resultString := fmt.Sprintf("%.2f", result)
				resultString += " грн."
				label4.SetText(resultString)
			}

		})

		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})
		window.Show()
	})
	if err != nil {
		panic(err)
	}
}
