package main

import (
	"fmt"
	"os"
)

//Product
type Product struct {
	name     string
	price    float64
	cost     Currency
	quantity int64
	producer string
	weight   float64
}

//Product constructor
func NewProduct(name string, price float64, cost Currency, quantity int64, producer string, weight float64) Product {
	return Product{name, price, cost, quantity, producer, weight}
}

// Set&Get for Name
func (f *Product) SetName(name string) {
	f.name = name
}

func (f Product) Name() string {
	return f.name
}

// Set&Get for Price
func (f *Product) SetPrice(price float64) {
	f.price = price
}

func (f Product) Price() float64 {
	return f.price
}

// Set&Get for Cost
func (f *Product) SetCost(cost Currency) {
	f.cost = cost
}

func (f Product) Cost() Currency {
	return f.cost
}

// Set&Get for Quantity
func (f *Product) SetQuantity(quantity int64) {
	f.quantity = quantity
}

func (f Product) Quantity() int64 {
	return f.quantity
}

// Set&Get for Producer
func (f *Product) SetProducer(producer string) {
	f.producer = producer
}

func (f Product) Producer() string {
	return f.producer
}

// Set&Get for Weight
func (f *Product) SetWeight(weight float64) {
	f.weight = weight
}

func (f Product) Weight() float64 {
	return f.weight
}

//вартість одиниці товару в гривнях
func (product Product) GetPriceIn() float64 {
	return product.Cost().ExRate * product.Price()
}

// загальна вартість усіх наявних товарів даного виду на складі (у гривнях)
func (product Product) GetTotalPrice() float64 {
	return float64(product.GetPriceIn()) * float64(product.Quantity())
}

// загальна вага усіх наявних товарів даного виду на складі
func (product Product) GetTotalWeight() float64 {
	return float64(product.Weight()) * float64(product.Quantity())
}

// Currency
type Currency struct {
	Name   string
	ExRate float64
}

//Currency constructor
func NewCurrency(Name string, ExRate float64) *Currency {
	c := new(Currency)
	c.Name = Name
	c.ExRate = ExRate
	return c
}

// назва валюти та її курс у гривнях, в строковому форматі
func (currency Currency) GetCurrency() string {
	tmp := fmt.Sprint(currency.ExRate)
	return string(currency.Name) + ": " + tmp + " грн."
}

func readProductsArray() []Product {
	var array []Product
	var n int
	fmt.Println("Введіть кількість продуктів: ")
	fmt.Fscan(os.Stdin, &n)

	var name string
	var price float64
	var costName string
	var costExRate float64
	var quantity int64
	var producer string
	var weight float64

	for i := 0; i < n; i++ {
		fmt.Println("Введіть через пробіл назву, ціну, назву валюти, курс валюти, кількість, компанію-виробника та вагу: ")
		_, err := fmt.Scan(&name)
		_, err = fmt.Scan(&price)
		_, err = fmt.Scan(&costName)
		_, err = fmt.Scan(&costExRate)
		_, err = fmt.Scan(&quantity)
		_, err = fmt.Scan(&producer)
		_, err = fmt.Scan(&weight)

		p := NewProduct(name, price, Currency{Name: costName, ExRate: costExRate}, quantity, producer, weight)

		if err == nil {
			array = append(array, p)
		} else {
			fmt.Printf("Було введено некоректні дані!\n")
			os.Exit(3)
		}

	}
	return array
}

func PrintProduct(p Product) {
	fmt.Println("Назва: " + p.Name())
	fmt.Printf("Ціна: %.2f\n", p.Price())
	fmt.Println("Валюта: " + p.Cost().Name)
	fmt.Printf("Курс валюти: %.2f\n", p.Cost().ExRate)
	fmt.Printf("Кількість: %d\n", p.Quantity())
	fmt.Println("Компанія-виробник: " + p.Producer())
	fmt.Printf("Вага: %.2f\n", p.Weight())
}

func PrintProducts(arr []Product) {
	for i := 0; i < len(arr); i++ {
		fmt.Printf("\nПродукт %d-й:", i+1)
		fmt.Println("")
		PrintProduct(arr[i])
	}
}

func GetProductsInfo(arr []Product) (min Product, max Product) {
	min = arr[0]
	max = arr[0]
	for i := 0; i < len(arr); i++ {
		if arr[i].GetPriceIn() < min.GetPriceIn() {
			min = arr[i]
		}
		if arr[i].GetPriceIn() > max.GetPriceIn() {
			max = arr[i]
		}
	}
	return min, max
}

func main() {
	p := readProductsArray()
	PrintProducts(p)
	min, max := GetProductsInfo(p)

	fmt.Println("\nПродукт з мінімальною вартістю:")
	PrintProduct(min)
	fmt.Println("\nПродукт з максимальною вартістю:")
	PrintProduct(max)
}
