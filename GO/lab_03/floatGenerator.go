package main

import (
	"fmt"
	"math"
)

func main() {
	var a float64 = 1103515245
	var c float64 = 12345
	var m float64 = float64(math.Pow(2, 31))
	var n float64 = 100
	var k float64 = 10000

	var x0 float64 = 23.5

	array := generateFloatNumbers(a, c, m, n, k, x0)
	fmt.Println(array)
}

func generateFloatNumbers(a float64, c float64, m float64, n float64, k float64, x0 float64) []float64 {
	var array []float64
	var arrayDecimal []int8
	var f0 int8 = 5
	arrayDecimal = append(arrayDecimal, f0)
	array = append(array, x0)
	var tmp float64 = x0
	var tmpDecimal int8 = f0

	for i := 1; float64(i) < k; i++ {
		x := math.Mod(a*tmp+c, m)
		f := int8((int64(a)*int64(tmpDecimal) + int64(c)) % int64(m))
		arrayDecimal = append(arrayDecimal, f/100)

		tmp = x
		tmpDecimal = f
		array = append(array, math.Mod(x, n)+float64(f)/100)
	}
	return array
}
