package main

import (
	"fmt"
	"math"
)

func main() {
	var a uint = 1103515245
	var c uint = 12345
	var m uint = uint(math.Pow(2, 31))
	var n uint = 100
	var k uint = 10000

	var x0 uint
	fmt.Println("Введіть х0: ")

	fmt.Scanf("%d", &x0)

	array := generateIntNumbers(a, c, m, n, k, x0)
	fmt.Println(array)
	//fmt.Println(len(array))

	arrayAnalysis(array, int(n), int(k))
}

func generateIntNumbers(a uint, c uint, m uint, n uint, k uint, x0 uint) []uint {
	var array []uint
	array = append(array, x0)
	tmp := x0
	for i := 1; uint(i) < k; i++ {
		x := (a*tmp + c) % m
		tmp = x
		array = append(array, x%n)
	}
	return array
}

func arrayAnalysis(arr []uint, n int, k int) {
	dictionary := make(map[int]int)
	//частота інтервалів
	fmt.Println("Частота інтервалів:")
	for i := 0; i < n; i++ {
		tmp := 0
		for _, element := range arr {
			if element == uint(i) {
				tmp += 1
			}
		}
		dictionary[i] = tmp
		fmt.Printf("[%d - %d): %d\n", i, i+1, tmp)
	}

	//статистична імовірність появи випадкових величин
	fmt.Println("Статистична імовірність появи випадкових величин:")
	for i := 0; i < n; i++ {
		fmt.Println(i, ": ", float64(dictionary[i])/float64(k))
	}

	//математичне сподівання випадкових величин
	fmt.Println("Математичне сподівання випадкових величин:")
	for i := 0; i < n; i++ {
		fmt.Println(i, ": ", float64(i)*float64(dictionary[i])/float64(k))
	}

	//дисперсія випадкових величин
	fmt.Println("Дисперсія випадкових величин:")
	for i := 0; i < n; i++ {
		fmt.Println(i, ": ", math.Pow(float64(i)-float64(i)*float64(dictionary[i])/float64(k), 2)*float64(dictionary[i])/float64(k))
	}

	//середньоквадратичне відхилення випадкових величин
	fmt.Println("Середньоквадратичне відхилення випадкових величин:")

	for i := 0; i < n; i++ {
		fmt.Println(i, ": ", math.Sqrt(math.Pow(float64(i)-float64(i)*float64(dictionary[i])/float64(k), 2)*float64(dictionary[i])/float64(k)))
	}
}
